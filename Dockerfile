FROM openjdk:11

LABEL maintainer="Phillip Tarrant <https://gitlab.com/Ptarrant1> and Dockerfile created by kevdagoat <https://gitlab.com/kevdagoat>"

ARG USER_UID=25565
ARG USER_GID=${USER_UID}
ARG USERNAME=crafty

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd -s /bin/bash --uid $USER_UID --gid $USER_GID -m $USERNAME

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y python3 python3-dev python3-pip libmariadb-dev
ENV DEBIAN_FRONTEND=dialog

COPY --chown=${USERNAME} ./ /crafty_web
WORKDIR /crafty_web

USER ${USERNAME}

RUN pip3 install -r requirements.txt

EXPOSE 8000
EXPOSE 25500-25600

USER root
RUN chown -R ${USERNAME} /crafty_web
USER ${USERNAME}

CMD ["python3", "crafty.py", "-c", "/crafty_web/configs/docker_config.yml"]

